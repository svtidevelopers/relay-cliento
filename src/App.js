import React, {Component} from 'react';
import logo from './contento.png';
import './App.css';
import Recommended from "./kokos";

class App extends React.Component {
    render() {
        return (
            <div className="App">
                <header className="App-header">
                    <img src={logo} className="App-logo" alt="logo"/>
                    <h1 className="App-title">Bienvenido a Cliento con Relay!</h1>
                </header>

                <Recommended/>
            </div>
        );
    }
}

export default App