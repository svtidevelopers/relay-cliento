import React, {Component} from 'react';
import {graphql, QueryRenderer} from 'react-relay';

import relay from '../relay'

class Recommended extends React.Component {
    render() {
        return (
            <QueryRenderer
                environment={relay}
                query={graphql`query kokosQuery { playContent { recommended {name, byline, description} } }`}
                variables={{
                    pageID: '110798995619330',
                }}

                render={({error, props}) => {

                    if (error) {
                        return <div>{error.message}</div>;
                    } else if (props) {
                        const recommendedList = props.playContent.recommended
                        return (
                            <div>
                                Vi rekommenderar
                                <div>
                                    {recommendedList.map(item =>
                                        <div key={item.name}>
                                            <div>{item.byline}</div>
                                            <div>{item.description}</div>
                                        </div>)}
                                </div>
                            </div>);
                    }
                    return <div>Loading</div>;
                }}
            />
        );
    }
}

export default Recommended;
