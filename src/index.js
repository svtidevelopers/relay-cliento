import React from 'react';
import ReactDOM from 'react-dom';
import { graphql } from 'relay-runtime';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();

//query={graphql:`query clientQuery {       viewer { id } } `};