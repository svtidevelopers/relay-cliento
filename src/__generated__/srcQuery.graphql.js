/**
 * @flow
 * @relayHash f2af27b53aa2942a5186c7a0ce18073d
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
export type srcQueryVariables = {| |};
export type srcQueryResponse = {|
  +playContent: {|
    +singles: $ReadOnlyArray<{|
      +id: string,
      +name: string,
    |}>,
  |},
|};
*/


/*
query srcQuery {
  playContent {
    singles {
      id
      name
    }
  }
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "kind": "LinkedField",
    "alias": null,
    "name": "playContent",
    "storageKey": null,
    "args": null,
    "concreteType": "PlayContent",
    "plural": false,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "singles",
        "storageKey": null,
        "args": null,
        "concreteType": "Single",
        "plural": true,
        "selections": [
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "id",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "name",
            "args": null,
            "storageKey": null
          }
        ]
      }
    ]
  }
];
return {
  "kind": "Request",
  "operationKind": "query",
  "name": "srcQuery",
  "id": null,
  "text": "query srcQuery {\n  playContent {\n    singles {\n      id\n      name\n    }\n  }\n}\n",
  "metadata": {},
  "fragment": {
    "kind": "Fragment",
    "name": "srcQuery",
    "type": "Query",
    "metadata": null,
    "argumentDefinitions": [],
    "selections": v0
  },
  "operation": {
    "kind": "Operation",
    "name": "srcQuery",
    "argumentDefinitions": [],
    "selections": v0
  }
};
})();
(node/*: any*/).hash = '944294005e7fa0860da06694b2c3c2ac';
module.exports = node;
